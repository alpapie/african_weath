from datetime import date, datetime
from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse
import json
import requests
# Create your views here.


def index(request):
    return render(request,'index.html')

def weather(request):
    city=request.POST.get('vil_name')
    data_se=requests.get('https://api.openweathermap.org/data/2.5/forecast?q='+city+'&appid=4d8fb5b93d4af21d66a2948710284366&units=metric&cnt=4')
    
    data=data_se.json()
    weather=data['list']
    for i in weather:
        i['dt']=datetime.fromtimestamp(i['dt']).strftime('%Y/%m/%d ')
        # i['date']=datetime.fromtimestamp(i['dt']).strftime('%D')
    weather0=weather[0]
    
    # weather0['dt_txt']=datetime.datetime.strptime(weather0['dt_txt'], "%Y-%m-%d")
    # return HttpResponse(weather,content_type='application/json')
    return render(request,'temp.html',{'data':data,"weather":weather,"weather0":weather0})